
Parse.Cloud.define("getPayPalToken", function(request, response){
	
	console.log(request);

    var basicAuthString = 'Basic ' + request.params.auth;

    Parse.Cloud.httpRequest({
        method:'POST',
        url: 'https://api.sandbox.paypal.com/v1/oauth2/token', 
        headers: {
            'Authorization':basicAuthString,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept-Language': 'en_US'
        },
        body:{
            grant_type:'client_credentials'
        },
        success: function(httpResponse) {
            console.log(httpResponse.text);
            response.success(httpResponse.text);
        },
        error: function(httpResponse) {
            console.error("Request failed with response code " + httpResponse.status);
            response.error(httpResponse.text);
        }
    });

});
Parse.Cloud.define("checkPayPalPayment", function(request, response){

    // var token = request.params.token;
	// var pid = request.params.pid;

    Parse.Cloud.httpRequest({
        method:'GET',
        url: 'https://api.sandbox.paypal.com/v1/payments/payment/' + request.params.pid, 
        headers: {
            'Authorization': 'Bearer ' + request.params.token,
            'Content-Type': 'application/json',
            'Accept-Language': 'en_US'
        },
        success: function(jsonresponse) {
            console.log(jsonresponse);
            response.success(jsonresponse.text);
        },
        error: function(jsonresponse) {
            console.error("Request failed with response code " + jsonresponse);
            response.error(jsonresponse);
        }
    });

});

Parse.Cloud.define("refundInsurance", function(request, response){

    // var token = request.params.token;

    Parse.Cloud.httpRequest({
        method:'POST',
        url: request.params.link,
		body: request.params.body,
        headers: {
            'Authorization': 'Bearer ' + request.params.token,
            'Content-Type': 'application/json',
            'Accept-Language': 'en_US'
        },
		success: function(jsonresponse) {
            console.log(jsonresponse);
            response.success(jsonresponse.text);
        },
        error: function(jsonresponse) {
            console.error("Request failed with response code " + jsonresponse);
            response.error(jsonresponse);
        }
    });

});
