package rush.bahama.com.bahamarush.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;

import rush.bahama.com.bahamarush.utils.Constants;

/**
 * Created by amitn on 25.06.2015
 */
public class PlaceModel extends BaseModel {

    private LatLng mPosition;
    private Bitmap mIcon;
    private String mDescription;
    private File mAudio;
    private String mTourId;
    private Marker mMarker;

    private PlaceModel(String parseId, int order, String tourId, LatLng position, String name, String description) {
        super(parseId, name, order);
        mTourId = tourId;
        mPosition = position;
        mDescription = description;
    }

    private void loadData(ParseFile icon, ParseFile audio, Context cnt) {
        loadIcon(icon, cnt);
        loadAudio(audio, cnt);
    }

    public LatLng getPosition() {
        return mPosition;
    }

    public Bitmap getIcon() {
        return mIcon;
    }

    private void setIcon(Bitmap bmp) {
        mIcon = bmp;
    }

    public String getDescription() {
        return mDescription;
    }

    public File getAudio() {
        return mAudio;
    }

    public static PlaceModel fromParseObject(ParseObject object, Context cnt) {
        ParseGeoPoint point = object.getParseGeoPoint(Constants.POSITION_KEY);
        PlaceModel res = new PlaceModel(
                object.getObjectId(),
                object.getInt(Constants.ORDER_KEY),
                object.getString(Constants.TOUR_ID_KEY),
                new LatLng(point.getLatitude(), point.getLongitude()),
                object.getString(Constants.NAME_KEY),
                object.getString(Constants.DESCRIPTION_KEY));
        res.loadData(object.getParseFile(Constants.ICON_KEY), object.getParseFile(Constants.AUDIO_KEY), cnt);
        return res;
    }

    private void loadAudio(ParseFile audioFile, Context cnt) {
        File cache = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + getCacheDir(cnt) + mParseId);
        if (!(cache.exists() || cache.mkdirs())) {
            return;
        }
        final File audio = new File(cache, Constants.AUDIO_FILE_NAME);
        if (audio.exists()) {
            mAudio = audio;
        } else {
            try {
                if (audioFile == null || !audio.createNewFile()) {
                    return;
                }
                audioFile.getDataInBackground(new GetDataCallback() {
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            try {
                                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(audio));
                                bos.write(data);
                                bos.flush();
                                bos.close();
                                PlaceModel.this.mAudio = audio;
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else {
                            Log.e("ERROR", "AUDIO: ", e);
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadIcon(ParseFile iconFile, Context cnt) {
        File cache = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + getCacheDir(cnt) + mParseId);
        if (!(cache.exists() || cache.mkdirs())) {
            return;
        }
        final File icon = new File(cache, Constants.ICON_FILE_NAME);
        if (icon.exists() && icon.length() > 0) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            setIcon(BitmapFactory.decodeFile(icon.getAbsolutePath(), options));
        } else {
            try {
                if (iconFile == null || !icon.createNewFile()) {
                    return;
                }
                iconFile.getDataInBackground(new GetDataCallback() {
                    public void done(byte[] data, ParseException e) {
                        if (e == null) {
                            setIcon(BitmapFactory.decodeByteArray(data, 0, data.length));
                            try {
                                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(icon));
                                bos.write(data);
                                bos.flush();
                                bos.close();
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        } else {
                            Log.e("ERROR", "AUDIO: ", e);
                        }
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getCacheDir(Context cnt) {
        return Constants.DEFAULT_DATA_DIR + cnt.getPackageName() + Constants.PLACES_CACHE_DIR;
    }

    public Marker getMarker() {
        return mMarker;
    }

    public void setMarker(Marker marker) {
        mMarker = marker;
    }

    public boolean isNearTo(Location currLoc) {
        float[] results = new float[1];
        Location.distanceBetween(mPosition.latitude, mPosition.longitude, currLoc.getLatitude(), currLoc.getLongitude(), results);
        return results[0] <= Constants.WATCH_RADIUS;
    }

    public static PlaceModel findByMarker(Collection<PlaceModel> list, @NonNull Marker m) {
        for (PlaceModel p : list) {
            if (m.equals(p.getMarker())) {
                return p;
            }
        }
        return null;
    }

    public String getTourId() {
        return mTourId;
    }

    @Override
    public boolean equals(Object o) {
        boolean isEqual = super.equals(o);
        if (!(o instanceof PlaceModel)) {
            return false;
        }
        PlaceModel bp = (PlaceModel) o;
        boolean descriptionsAreEqual;
        if (mDescription == null) {
            descriptionsAreEqual = bp.mDescription == null;
        } else {
            descriptionsAreEqual = mDescription.equals(bp.mDescription);
        }
        return isEqual && descriptionsAreEqual;
    }

    @Override
    public int hashCode() {
        return super.hashCode() + (mDescription == null ? 0 : mDescription.hashCode());
    }
}
