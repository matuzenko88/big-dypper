package rush.bahama.com.bahamarush.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import rush.bahama.com.bahamarush.R;

/**
 * Created by amitn on 15.07.2015
 */
public abstract class ToolbarActivity extends AppCompatActivity {

    @Bind(R.id.my_toolbar)
    Toolbar mToolbar;
    @Bind(R.id.toolbar_title)
    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(null);
        mTitle.setText(title);
    }

    protected abstract int getLayoutId();
}
