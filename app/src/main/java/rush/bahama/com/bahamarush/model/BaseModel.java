package rush.bahama.com.bahamarush.model;

import android.support.annotation.NonNull;

/**
 * Created by amitn on 06.07.2015
 */
public abstract class BaseModel implements Comparable<BaseModel> {

    protected String mParseId;
    protected String mName;
    protected int mOrder;

    protected BaseModel(@NonNull String parseId, String name, int order) {
        mParseId = parseId;
        mName = name;
        mOrder = order;
    }

    @Override
    public int compareTo(@NonNull BaseModel another) {
        return mOrder - another.mOrder;
    }

    public String getParseId() {
        return mParseId;
    }

    public String getName() {
        return mName;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof BaseModel){
            BaseModel bo = (BaseModel)o;
            return mParseId.equals(bo.mParseId) && mOrder == bo.mOrder;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return mParseId.hashCode() + (mOrder * 17);
    }

    public String getDescription() {
        return null;
    }
}
