package rush.bahama.com.bahamarush.model;

import com.parse.ParseObject;

import rush.bahama.com.bahamarush.utils.Constants;

/**
 * Created by amitn on 06.07.2015
 */
public class TourModel extends BaseModel {

    private String mRouteId;
    private String mDescription;
    //private int mDuration;


    private TourModel(String id, int order, String name, String routeId, String description/*, int duration*/) {
        super(id, name, order);
        mRouteId = routeId;
        mDescription = description;
        //mDuration = duration;
    }

    public static TourModel fromParseObject(ParseObject o) {
        return new TourModel(o.getObjectId(),
                o.getInt(Constants.ORDER_KEY),
                o.getString(Constants.NAME_KEY),
                o.getString(Constants.ROUTE_ID_KEY),
                o.getString(Constants.DESCRIPTION_KEY)
                /*, o.getInt(Constants.DURATION_KEY)*/);
    }

    @Override
    public String getName() {
        return mName /*+ (mDuration > 0 ? (" (" + Constants.getDurationString(TimeUnit.MINUTES.toSeconds(mDuration), false) + ")") : "")*/;
    }

    /*public String getShortName() {
        return mName;
    }*/

    @Override
    public String getDescription() {
        return mDescription;
    }

    public String getRouteId() {
        return mRouteId;
    }

    /*public int getDuration() {
        return mDuration;
    }*/
}
