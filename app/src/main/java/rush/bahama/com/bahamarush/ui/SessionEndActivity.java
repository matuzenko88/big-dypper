package rush.bahama.com.bahamarush.ui;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.paypal.android.sdk.payments.PayPalService;

import butterknife.Bind;
import butterknife.OnClick;
import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.utils.OnPayPalActionCallback;

/**
 * Created by amitn on 16.07.2015
 */
public class SessionEndActivity extends ServiceListenerActivity {

    @Bind(R.id.et_pwd)
    EditText mPwdET;
    @Bind(R.id.tv_amount)
    TextView mAmount;
    @Bind(R.id.til)
    TextInputLayout mTil;

    @Override
    protected void onServiceConnected() {
        mAmount.setText(getString(R.string.you_owe) + " " + mService.getTotalAmount() + getString(R.string.usd_sign));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.thank_you));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_end_tour;
    }

    @OnClick(R.id.btn_confirm_pwd)
    void confirmPassClick() {
        if (!mPwdET.getText().toString().equals(PreferenceManager.getDefaultSharedPreferences(this).getString(Constants.KEY_PWD, getString(R.string.default_password)))) {
            mTil.setError(getString(R.string.incorrect_pwd));
        } else {
            final int amount = mService.getRefundableAmount();
            new MaterialDialog.Builder(this)
                    .title(getString(R.string.end_session_title))
                    .content(getString(R.string.end_session_1) + " " + amount + getString(R.string.usd_sign) + " " + getString(R.string.end_session_2))
                    .positiveColorRes(R.color.orange_dark)
                    .negativeColorRes(R.color.orange_dark)
                    .positiveText(getString(R.string.finish))
                    .negativeText(R.string.cancel)
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                            refundCosts(amount);
                        }
                    })
                    .show();
        }
    }

    private void refundCosts(int amount) {
        PayPalService.clearAllUserData(this);
        if (mService != null) {
            final MaterialDialog d = new MaterialDialog.Builder(this)
                    .cancelable(false)
                    .progress(true, 0)
                    .content(getString(R.string.msg_refunding) + amount + getString(R.string.usd_sign) + ")")
                    .show();
            mService.endCurrentSession(new OnPayPalActionCallback() {

                @Override
                public void onSuccess() {
                    d.dismiss();
                    setResult(RESULT_OK);
                    finish();
                }

                @Override
                public void onError(String error) {
                    d.dismiss();
                    informError(error);
                }
            });
        } else {
            informError(getString(R.string.err_service_not_strated));
        }
    }
}
