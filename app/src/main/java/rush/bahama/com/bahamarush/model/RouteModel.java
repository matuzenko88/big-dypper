package rush.bahama.com.bahamarush.model;

import com.parse.ParseObject;

import rush.bahama.com.bahamarush.utils.Constants;

/**
 * Created by amitn on 06.07.2015
 */
public class RouteModel extends BaseModel {

    private RouteModel(String id, int order, String name) {
        super(id, name, order);
    }

    public static RouteModel fromParseObject(ParseObject o){
        return new RouteModel(o.getObjectId(), o.getInt(Constants.ORDER_KEY), o.getString(Constants.NAME_KEY));
    }

}
