package rush.bahama.com.bahamarush.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;

import butterknife.OnClick;
import rush.bahama.com.bahamarush.core.MainService;
import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.core.MyApplication;
import rush.bahama.com.bahamarush.model.BaseModel;
import rush.bahama.com.bahamarush.model.DeviceModel;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.utils.OnPayPalActionCallback;
import rush.bahama.com.bahamarush.utils.OnRegistrationListener;

/**
 * Created by amitn on 06.07.2015
 */
public class SplashActivity extends ServiceListenerActivity<BaseModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.app_name));
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        isVendorRegistered(prefs);
        findViewById(R.id.btn_start).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVendorRegistered(prefs)) {
                    if (!mService.isSessionActive()) {
                        mService.getPayPalToken();
                        PayPalPayment payment = new PayPalPayment(new BigDecimal(Constants.USD_FULL_INSURANCE_COST), "USD", "Device insurance payment",
                                PayPalPayment.PAYMENT_INTENT_SALE);
                        Intent intent = new Intent(SplashActivity.this, PaymentActivity.class);
                        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, MyApplication.getPayPalConfig());
                        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
                        startActivityForResult(intent, 0);
                    } else {
                        Intent i = new Intent(SplashActivity.this, DataListActivity.class);
                        i.putExtra(Constants.EXTRA_TYPE, MainService.ObjectType.ROUTE);
                        startActivity(i);
                        finish();
                    }
                }
            }
        });
        startPayPalService();
    }

    private void startPayPalService() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, MyApplication.getPayPalConfig());
        startService(intent);
    }


    @SuppressLint("CommitPrefEdits")
    private boolean isVendorRegistered(SharedPreferences prefs) {
        if (prefs.getInt(Constants.SESSION_TIME, 0) == 0) {
            askUserToRegister();
            return false;
        }
        return true;
    }

    private void askUserToRegister() {
        new MaterialDialog.Builder(this)
                .title("Registration")
                .cancelable(false)
                .autoDismiss(false)
                .positiveText(R.string.ok)
                .positiveColorRes(R.color.orange_dark)
                .customView(R.layout.dialog_register, true)
                .callback(new MaterialDialog.ButtonCallback() {
                              @Override
                              public void onPositive(final MaterialDialog dialog) {
                                  super.onPositive(dialog);
                                  ViewGroup c = (ViewGroup) dialog.getCustomView();
                                  if (c != null) {
                                      final MaterialDialog progress = showProgress();
                                      DeviceModel m = new DeviceModel();
                                      for (int i = 0; i < c.getChildCount(); i++) {
                                          TextInputLayout til = (TextInputLayout) c.getChildAt(i);
                                          EditText et = til.getEditText();
                                          String value = et.getText().toString();
                                          switch (et.getId()) {
                                              case R.id.et_session_time:
                                                  if (!value.isEmpty()) {
                                                      m.setMaxHours(Integer.parseInt(value));
                                                  }
                                                  break;
                                              case R.id.et_email:
                                                  m.setEmail(value);
                                                  break;
                                              case R.id.et_vendor:
                                                  m.setVendorName(value);
                                                  break;
                                              case R.id.et_location:
                                                  m.setVendorLocation(value);
                                                  break;
                                              case R.id.et_new_pass:
                                                  m.setPwd(value);
                                                  break;
                                              case R.id.et_confirm_pass:
                                                  m.setPwdConfirm(value);
                                                  break;
                                              default:
                                                  break;
                                          }
                                      }
                                      m.save(SplashActivity.this, new OnRegistrationListener() {
                                          @Override
                                          public void onRegistrationFailed() {
                                              progress.dismiss();
                                              Toast.makeText(SplashActivity.this, getString(R.string.reg_error_msg), Toast.LENGTH_LONG).show();
                                          }

                                          @Override
                                          public void onRegistrationDone() {
                                              progress.dismiss();
                                              dialog.dismiss();
                                          }
                                      });
                                  }
                              }
                          }

                ).show();
    }

    private MaterialDialog showProgress() {
        return new MaterialDialog.Builder(this)
                .cancelable(false)
                .progress(true, 0)
                .content(getString(R.string.msg_registration))
                .show();
    }

    @OnClick(R.id.btn_pwd_settings_cog)
    void showChangePwdDialog() {
        class PasswordHolder {

            TextInputLayout mOldPass;
            TextInputLayout mNewPass;
            TextInputLayout mConfirmPass;

            PasswordHolder(View v) {
                mOldPass = (TextInputLayout) v.findViewById(R.id.til_old_pass);
                mNewPass = (TextInputLayout) v.findViewById(R.id.til_new_pass);
                mConfirmPass = (TextInputLayout) v.findViewById(R.id.til_confirm_pass);
            }

            void setError(TextInputLayout til, String error) {
                til.setError(error);
                if (error != null) {
                    til.getEditText().setText("");
                    til.getEditText().requestFocus();
                }
            }

            public boolean savePwd() {
                String oldPass = mOldPass.getEditText().getText().toString();
                String newPass = mNewPass.getEditText().getText().toString();
                String confirmPass = mConfirmPass.getEditText().getText().toString();
                if (!oldPass.equals(PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString(Constants.KEY_PWD, getString(R.string.default_password)))) {
                    setError(mOldPass, getString(R.string.incorrect_pwd));
                    return false;
                } else {
                    setError(mOldPass, null);
                }
                if (newPass.length() < 6) {
                    setError(mNewPass, getString(R.string.password_length_warning));
                    return false;
                } else {
                    if (newPass.equals(oldPass)) {
                        setError(mNewPass, getString(R.string.same_password_warning));
                        return false;
                    } else {
                        setError(mNewPass, null);
                    }
                }
                if (!confirmPass.equals(newPass)) {
                    setError(mConfirmPass, getString(R.string.not_match_password_warning));
                    return false;
                }
                PreferenceManager.getDefaultSharedPreferences(SplashActivity.this)
                        .edit()
                        .putString(Constants.KEY_PWD, mNewPass.getEditText().getText().toString())
                        .commit();
                return true;
            }
        }
        new MaterialDialog.Builder(this)
                .title(R.string.change_pass_header)
                .cancelable(false)
                .autoDismiss(false)
                .positiveText(R.string.change)
                .negativeText(R.string.cancel)
                .positiveColorRes(R.color.orange_dark)
                .negativeColorRes(R.color.orange_dark)
                .customView(R.layout.dialog_pass_change, true)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        PasswordHolder vh = new PasswordHolder(dialog.getCustomView());
                        if (vh.savePwd()) {
                            dialog.dismiss();
                            new MaterialDialog.Builder(SplashActivity.this)
                                    .title(R.string.password_changed_header)
                                    .content(R.string.password_set_message)
                                    .positiveText(R.string.ok)
                                    .positiveColorRes(R.color.orange_dark)
                                    .show();

                        }
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onServiceConnected() {
        if (mService.isSessionActive()) {
            startActivity(new Intent(this, TourActivity.class));
            finish();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
            if (confirm != null) {
                try {
                    checkPaymentAndStart(confirm.toJSONObject().getJSONObject("response").getString("id"));
                } catch (JSONException e) {
                    informError(getString(R.string.err_json) + " " + e.getMessage());
                }
            } else {
                informError(getString(R.string.err_server_answer));
            }
        } else if (resultCode == RESULT_CANCELED) {
            informError(getString(R.string.msg_payment_canceled));
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            informError(getString(R.string.msg_wrong_payment));
        }
    }

    private void checkPaymentAndStart(String id) {
        final MaterialDialog d = new MaterialDialog.Builder(this).cancelable(false).progress(true, 0).content(getString(R.string.msg_check_payment)).show();
        mService.checkPayPalPayment(id, new OnPayPalActionCallback() {
            @Override
            public void onSuccess() {
                d.dismiss();
                Intent i = new Intent(SplashActivity.this, DataListActivity.class);
                i.putExtra(Constants.EXTRA_TYPE, MainService.ObjectType.ROUTE);
                startActivity(i);
                finish();
            }

            @Override
            public void onError(String error) {
                d.dismiss();
                informError(error);
            }
        }, true);
    }
}
