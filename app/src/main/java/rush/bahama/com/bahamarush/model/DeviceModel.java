package rush.bahama.com.bahamarush.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.kristijandraca.backgroundmaillibrary.BackgroundMail;
import com.parse.ParseException;
import com.parse.ParseObject;

import rush.bahama.com.bahamarush.core.MyApplication;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.utils.OnRegistrationListener;

/**
 * Created by amitn on 22.07.2015
 */
public class DeviceModel {

    private String mEmail;
    private String mVendorName;
    private String mVendorLocation;
    private String mPwd;
    private int mMaxHours;
    private String mPwdConfirm;

    public void save(final Context cnt, final OnRegistrationListener l) {
        if (mMaxHours <= 0 ||
                mEmail.isEmpty() ||
                mVendorName.isEmpty() ||
                mVendorLocation.isEmpty() ||
                mPwd.isEmpty() ||
                mPwd.length() != 6 ||
                mPwdConfirm.isEmpty() ||
                mPwdConfirm.length() != 6 ||
                !mEmail.contains("@") ||
                !mPwdConfirm.equals(mPwd)) {
            l.onRegistrationFailed();
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                sendMail(cnt);
                saveToParse();
                saveToPrefs(cnt);
                l.onRegistrationDone();
            }
        }).start();
    }

    public void saveToParse() {
        ParseObject o = new ParseObject(Constants.CUSTOMER_DEVICE_CLASS);
        o.put(Constants.NAME_KEY, mVendorName);
        o.put(Constants.EMAIL_KEY, mEmail);
        o.put(Constants.LOCATION_KEY, mVendorLocation);
        o.put(Constants.SESSION_TIME, mMaxHours);
        o.put(Constants.DEVICE_ID_KEY, MyApplication.DEVICE_ID);
        try {
            o.save();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void saveToPrefs(Context cnt) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(cnt).edit();
        editor.putString(Constants.NAME_KEY, mVendorName);
        editor.putString(Constants.EMAIL_KEY, mEmail);
        editor.putString(Constants.LOCATION_KEY, mVendorLocation);
        editor.putString(Constants.KEY_PWD, mPwd);
        editor.putInt(Constants.SESSION_TIME, mMaxHours);
        editor.putString(Constants.DEVICE_ID_KEY, MyApplication.DEVICE_ID);
        editor.apply();
    }

    private void sendMail(Context cnt) {
        BackgroundMail bm = new BackgroundMail(cnt);
        bm.setGmailUserName("island.tour.app@gmail.com");
        bm.setGmailPassword("Po5xErZUcHl2");
        bm.setMailTo("y2kristi@hotmail.com," + mEmail);
        bm.setFormSubject("New Island Tour registration");
        bm.setProcessVisibility(false);
        bm.setFormBody(getBody());
        bm.send();
    }

    public String getBody() {
        return "Vendor: " + mVendorName +
                "\n Location: " + mVendorLocation +
                "\n Email: " + mEmail +
                "\nDevice ID: " + MyApplication.DEVICE_ID;
    }

    public void setEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public void setVendorName(String mVendorName) {
        this.mVendorName = mVendorName;
    }

    public void setVendorLocation(String mVendorLocation) {
        this.mVendorLocation = mVendorLocation;
    }

    public void setPwd(String mPwd) {
        this.mPwd = mPwd;
    }

    public void setPwdConfirm(String mPwdC) {
        this.mPwdConfirm = mPwdC;
    }

    public void setMaxHours(int mMaxHours) {
        this.mMaxHours = mMaxHours;
    }
}
