package rush.bahama.com.bahamarush.utils;

/**
 * Created by amitn on 29.07.2015
 */
public interface OnRegistrationListener {
    void onRegistrationFailed();
    void onRegistrationDone();
}
