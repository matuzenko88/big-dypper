package rush.bahama.com.bahamarush.ui;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.MediaController;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.model.PlaceModel;
import rush.bahama.com.bahamarush.utils.Constants;

/**
 * Created by amitn on 15.07.2015
 */
public class PlaceDetailsActivity extends ServiceListenerActivity implements MediaPlayer.OnPreparedListener, MediaController.MediaPlayerControl {

    private String mPlaceId;
    @Bind(R.id.header_view)
    ImageView mHeaderImage;
    @Bind(R.id.tv_description)
    TextView mDescription;
    @Bind(R.id.player_anchor)
    View mAnchorView;

    private MediaPlayer mPlayer;
    private MediaController mController;
    private Handler myHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(Constants.EXTRA_PLACE_ID)) {
            mPlaceId = extras.getString(Constants.EXTRA_PLACE_ID);
        }
    }

    @Override
    protected void onServiceConnected() {
        PlaceModel mPlace = mService.findPlace(mPlaceId);
        if (mPlace == null) {
            Toast.makeText(PlaceDetailsActivity.this, getString(R.string.empty_route), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        setTitle(mPlace.getName());
        mHeaderImage.setImageBitmap(mPlace.getIcon());
        mTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mController != null) {
                    if (mController.isShowing()) {
                        mController.hide();
                    } else {
                        mController.show();
                    }
                }
            }
        });
        mDescription.setText(mPlace.getDescription());
        File audioFile = mPlace.getAudio();
        if (audioFile != null && audioFile.exists()) {
            mPlayer = new MediaPlayer();
            mPlayer.setOnPreparedListener(this);
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (mController != null) {
                        mController.hide();
                        showInfoToast();
                    }
                }
            });
            mController = new MediaController(this, false) {
                @Override
                public void show(int timeout) {
                    super.show(0);
                }
            };
            try {
                mPlayer.setDataSource(this, Uri.fromFile(audioFile));
                mPlayer.prepare();
                mPlayer.start();
            } catch (IOException e) {
                Log.e("PLAYER", "Could not open file " + audioFile + " for playback.", e);
            }
        }
    }

    public void showInfoToast() {
        Toast t = Toast.makeText(PlaceDetailsActivity.this, getString(R.string.player_controls_message), Toast.LENGTH_SHORT);
        t.setGravity(Gravity.TOP, 0, 0);
        t.show();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_place;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mController != null) {
            mController.hide();
        }
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
        }
    }

    //--MediaPlayerControl methods----------------------------------------------------
    public void start() {
        if (mPlayer != null) {
            mPlayer.start();
        }
    }

    public void pause() {
        if (mPlayer != null) {
            mPlayer.pause();
        }
    }

    public int getDuration() {
        if (mPlayer != null) {
            return mPlayer.getDuration();
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (mPlayer != null) {
            return mPlayer.getCurrentPosition();
        }
        return 0;
    }

    public void seekTo(int i) {
        if (mPlayer != null) {
            mPlayer.seekTo(i);
        }
    }

    public boolean isPlaying() {
        return mPlayer != null && mPlayer.isPlaying();
    }

    public int getBufferPercentage() {
        return 0;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }

    @Override
    public int getAudioSessionId() {
        return 0;
    }
    //--------------------------------------------------------------------------------

    public void onPrepared(MediaPlayer mediaPlayer) {
        if (mController != null) {
            mController.setMediaPlayer(this);
            mController.setAnchorView(mAnchorView);
            myHandler.post(new Runnable() {
                public void run() {
                    mController.setEnabled(true);
                    mController.show();
                    showInfoToast();
                }
            });
        }
    }
}
