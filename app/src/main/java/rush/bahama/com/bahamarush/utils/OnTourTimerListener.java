package rush.bahama.com.bahamarush.utils;

/**
 * Created by amitn on 21.07.2015
 */
public interface OnTourTimerListener {

    void onTick(String time, boolean outOfTime);
    void onTimeIsOver(int hoursOver);

}
