package rush.bahama.com.bahamarush.utils;

/**
 * Created by amitn on 23.07.2015
 */
public interface OnPayPalActionCallback {

    void onSuccess();
    void onError(String error);
}
