package rush.bahama.com.bahamarush.ui;

import android.content.Intent;
import android.graphics.Color;
import android.location.Location;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.location.LocationListener;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.paypal.android.sdk.payments.PayPalService;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rush.bahama.com.bahamarush.core.MainService;
import rush.bahama.com.bahamarush.core.MyApplication;
import rush.bahama.com.bahamarush.model.TourModel;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.model.PlaceModel;
import rush.bahama.com.bahamarush.utils.OnTourTimerListener;

public class TourActivity extends ServiceListenerActivity<PlaceModel> implements LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnTourTimerListener {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private GoogleApiClient mGoogleApiClient;
    private List<PlaceModel> mPlaces;
    private boolean mRequestingLocationUpdates;
    private LatLng mCurrentPosition;
    private int mCurrentPlaceToSeeIndex = -1;
    private long mLastCurrentRouteTime;
    private Polyline mLastCurrentRoute;
    private TourModel mTour;

    @Bind(R.id.timer)
    TextView mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        if (!setUpMapIfNeeded()) {
            Toast.makeText(this, getString(R.string.no_gp_abort), Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        buildGoogleApiClient();
        startPayPalService();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_tour;
    }

    private void loadCurrentRoute() {
        if (mCurrentPosition != null && mPlaces != null && !mPlaces.isEmpty() && mCurrentPlaceToSeeIndex > -1 && routeTimeout()) {
            mLastCurrentRouteTime = System.currentTimeMillis();
            downloadRoute(mCurrentPosition, mPlaces.get(mCurrentPlaceToSeeIndex).getPosition(), true);
        }
    }

    private boolean routeTimeout() {
        return System.currentTimeMillis() - mLastCurrentRouteTime > Constants.CURRENT_ROUTE_REFRESH_TIME;
    }

    private void buildRoute() {
        for (int i = mCurrentPlaceToSeeIndex + 1; i < mPlaces.size(); i++) {
            downloadRoute(mPlaces.get(i - 1).getPosition(), mPlaces.get(i).getPosition(), false);
        }
    }

    private void downloadRoute(final LatLng from, final LatLng to, final boolean current) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Document doc = getDocument(from, to);
                if (!current) {
                    if (doc == null) {
                        doc = getCachedDoc(from, to);
                    } else {
                        cacheDoc(from, to, doc);
                    }
                }
                if (doc != null) {
                    ArrayList<LatLng> directionPoint = getDirection(doc);
                    final PolylineOptions rectLine = new PolylineOptions().width(10).color(
                            Color.BLUE);

                    for (int i = 0; i < directionPoint.size(); i++) {
                        rectLine.add(directionPoint.get(i));
                    }
                    TourActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Polyline line = mMap.addPolyline(rectLine);
                            if (current) {
                                if (mLastCurrentRoute != null) {
                                    mLastCurrentRoute.remove();
                                }
                                mLastCurrentRoute = line;
                            }
                        }
                    });
                }
            }
        }).start();
    }

    private Document getCachedDoc(LatLng from, LatLng to) {
        File cache = getRoutesCacheFile(from, to);
        if (cache == null) {
            return null;
        }
        if (cache.exists()) {
            try {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                return dBuilder.parse(cache);
            } catch (ParserConfigurationException | SAXException | IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private void cacheDoc(LatLng from, LatLng to, Document doc) {
        File cache = getRoutesCacheFile(from, to);
        if (cache != null && (!cache.exists() || cache.delete())) {
            try {
                if (cache.createNewFile()) {
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();
                    Result output = new StreamResult(cache);
                    Source input = new DOMSource(doc);
                    transformer.transform(input, output);
                }
            } catch (TransformerException | IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File getRoutesCacheFile(LatLng from, LatLng to) {
        File cacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + Constants.DEFAULT_DATA_DIR
                + getPackageName()
                + Constants.ROUTES_CACHE_DIR);
        if (cacheDir.exists() || cacheDir.mkdirs()) {
            return new File(cacheDir, "" +
                    from.latitude
                    + '_'
                    + from.longitude
                    + '_'
                    + to.latitude
                    + '_'
                    + to.longitude
                    + Constants.XML);
        } else {
            return null;
        }
    }

    private void updatePlaces() {
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                PlaceModel p = PlaceModel.findByMarker(mPlaces, marker);
                if (p != null) {
                    Intent i = new Intent(TourActivity.this, PlaceDetailsActivity.class);
                    i.putExtra(Constants.EXTRA_PLACE_ID, p.getParseId());
                    startActivity(i);
                }
                return true;
            }
        });
        //final LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (PlaceModel p : mPlaces) {
            Marker m = addMarker(new MarkerOptions().position(p.getPosition()).title(p.getName()));
            p.setMarker(m);
            /*if (m != null) {
                builder.include(m.getPosition());
            }*/
        }
        /*getWindow().getDecorView().post(new Runnable() {
            @Override
            public void run() {
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 200));
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (setUpMapIfNeeded()) {
            if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
                startLocationUpdates();
            }
            if (mCurrentPosition != null) {
                zoomToMe();
            }
        }
    }

    private void zoomToMe() {
        if (mMap != null) {
            CameraUpdate myLoc = CameraUpdateFactory.newLatLngZoom(mCurrentPosition, 14);
            mMap.animateCamera(myLoc);
        }
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated..
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private boolean setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setTiltGesturesEnabled(true);
            }
        }
        return mMap != null;
    }

    private Marker addMarker(MarkerOptions options) {
        if (mMap != null) {
            return mMap.addMarker(options);
        }
        return null;
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.e("API CLIENT", "CONNECTED");
        Log.e("LOCATION UPDATE", "ON CONNECTED");
        updateMyLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
        if (!mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("SUSPENDED", "" + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("FAILED", connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("LOCATION UPDATE", "FROM CHANGED");
        updateMyLocation(location);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void startLocationUpdates() {
        Log.e("LOCATION UPDATES", "START");
        LocationRequest request = LocationRequest.create()
                .setFastestInterval(1000).setInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, request, this);
        mRequestingLocationUpdates = true;
    }

    protected void stopLocationUpdates() {
        if (mRequestingLocationUpdates) {
            Log.e("LOCATION UPDATES", "STOP");
            mRequestingLocationUpdates = false;
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }
        }
    }

    private void updateMyLocation(Location location) {
        if (location != null) {
            Log.e("LOCATION", location.toString());
            boolean zoom = mCurrentPosition == null;
            mCurrentPosition = new LatLng(location.getLatitude(), location.getLongitude());
            if (zoom) {
                zoomToMe();
            }
            checkNearestPlace(location);
            loadCurrentRoute();
        }
    }

    private void checkNearestPlace(@NonNull Location currLoc) {
        if (mPlaces != null && !mPlaces.isEmpty()) {
            for (int i = 0; i < mPlaces.size(); i++) {
                PlaceModel p = mPlaces.get(i);
                if (p.isNearTo(currLoc)) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(p.getPosition(), 12));
                    p.getMarker().showInfoWindow();
                    mCurrentPlaceToSeeIndex = i + 1;
                    return;
                }
            }
        }
    }

    @SuppressWarnings("deprecation")
    public Document getDocument(LatLng start, LatLng end) {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpContext localContext = new BasicHttpContext();
            HttpPost httpPost = new HttpPost(Constants.gMapsURL(start, end));
            HttpResponse response = httpClient.execute(httpPost, localContext);
            InputStream in = response.getEntity().getContent();
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            return builder.parse(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<LatLng> getDirection(Document doc) {
        NodeList nl1, nl2, nl3;
        ArrayList<LatLng> listGeoPoints = new ArrayList<>();
        nl1 = doc.getElementsByTagName("step");
        if (nl1.getLength() > 0) {
            for (int i = 0; i < nl1.getLength(); i++) {
                Node node1 = nl1.item(i);
                nl2 = node1.getChildNodes();

                Node locationNode = nl2
                        .item(getNodeIndex(nl2, "start_location"));
                nl3 = locationNode.getChildNodes();
                Node latNode = nl3.item(getNodeIndex(nl3, "lat"));
                double lat = Double.parseDouble(latNode.getTextContent());
                Node lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                double lng = Double.parseDouble(lngNode.getTextContent());
                listGeoPoints.add(new LatLng(lat, lng));

                locationNode = nl2.item(getNodeIndex(nl2, "polyline"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "points"));
                ArrayList<LatLng> arr = decodePoly(latNode.getTextContent());
                for (int j = 0; j < arr.size(); j++) {
                    listGeoPoints.add(new LatLng(arr.get(j).latitude, arr.get(j).longitude));
                }
                locationNode = nl2.item(getNodeIndex(nl2, "end_location"));
                nl3 = locationNode.getChildNodes();
                latNode = nl3.item(getNodeIndex(nl3, "lat"));
                lat = Double.parseDouble(latNode.getTextContent());
                lngNode = nl3.item(getNodeIndex(nl3, "lng"));
                lng = Double.parseDouble(lngNode.getTextContent());
                listGeoPoints.add(new LatLng(lat, lng));
            }
        }

        return listGeoPoints;
    }

    private int getNodeIndex(NodeList nl, String nodeName) {
        for (int i = 0; i < nl.getLength(); i++) {
            if (nl.item(i).getNodeName().equals(nodeName))
                return i;
        }
        return -1;
    }

    private ArrayList<LatLng> decodePoly(String encoded) {
        ArrayList<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;
        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dLat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dLat;
            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng position = new LatLng((double) lat / 1E5, (double) lng / 1E5);
            poly.add(position);
        }
        return poly;
    }

    @Override
    public String getParentObjectId() {
        return mTour == null ? null : mTour.getParseId();
    }

    @Override
    public void onDataLoaded(List<PlaceModel> places) {
        if (places != null && !places.isEmpty()) {
            mPlaces = places;
            Collections.sort(mPlaces);
            mCurrentPlaceToSeeIndex = 0;
            updatePlaces();
            loadCurrentRoute();
            buildRoute();
        } else {
            Toast.makeText(TourActivity.this, getString(R.string.no_sights_error), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void startPayPalService() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, MyApplication.getPayPalConfig());
        startService(intent);
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @OnClick(R.id.btn_new_tour)
    public void newTourClick() {
        new MaterialDialog.Builder(this)
                .content(R.string.new_tour_warning)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .positiveColorRes(R.color.orange_dark)
                .negativeColorRes(R.color.orange_dark)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        startNewTour();
                    }
                })
                .show();
    }

    private void startNewTour() {
        mService.endCurrentTour();
        Intent i = new Intent(TourActivity.this, DataListActivity.class);
        i.putExtra(Constants.EXTRA_TYPE, MainService.ObjectType.ROUTE);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.btn_finish_tour)
    public void finishTourClick() {
        MainService.showFinishTourDialog(this);
    }

    @Override
    protected void onServiceConnected() {
        if (!mService.isInitiated()) {
            mService.initData(this);
        } else if (!mService.isLoading()) {
            initTour();
        }
    }

    private void initTour() {
        mTour = mService.getCurrentTour();
        if (mTour != null) {
            setTitle(mTour.getName());
            mService.setOnDataLoadedListener(MainService.ObjectType.PLACE, this);
            mService.setTimerListener(this);
        } else {
            startNewTour();
        }
    }

    @Override
    public void onDataBuilt() {
        super.onDataBuilt();
        if (!mService.isLoading()) {
            initTour();
        }
    }

    @Override
    public void onBackPressed() {
        // intentionally do nothing
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.INTENT_REQUEST_FINISH_SESSION) {
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, SplashActivity.class));
                finish();
            }
        }
    }

    @Override
    public void onTick(String time, boolean outOfTime) {
        mTimer.setTextColor(getResources().getColor(outOfTime ? R.color.orange_light : android.R.color.white));
        mTimer.setText(time);
    }

    @Override
    public void onTimeIsOver(final int hours) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    new MaterialDialog.Builder(TourActivity.this)
                            .title(R.string.time_over_header)
                            .cancelable(false)
                            .content(hours <= Constants.MAX_HOURS_OVERTIME ?
                                    (getString(R.string.time_over_message)
                                            + " " + getString(R.string.msg_billed_1) + " " + Constants.USD_PER_HOUR_OVERTIME + getString(R.string.msg_billed_2) + " "
                                            + (Constants.MAX_HOURS_OVERTIME - hours)
                                            + getString(R.string.msg_billed_3))
                                    :
                                    getString(R.string.msg_overtime)).positiveText(R.string.ok).callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            super.onPositive(dialog);
                        }
                    })
                            .positiveColorRes(R.color.orange_dark).show();
                } catch (MaterialDialog.DialogException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
