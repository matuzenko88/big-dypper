package rush.bahama.com.bahamarush.utils;

/**
 * Created by amitn on 08.07.2015
 */
public interface OnDataBuildListener {

    void onStartBuildingData();

    void onDataBuilt();
}
