package rush.bahama.com.bahamarush.utils;

import java.util.List;

import rush.bahama.com.bahamarush.model.BaseModel;

/**
 * Created by amitn on 06.07.2015
 */
public interface OnDataLoadedListener<T extends BaseModel> {

    void onDataLoaded(List<T> items);

    String getParentObjectId();

}
