package rush.bahama.com.bahamarush.ui;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.core.MainService;
import rush.bahama.com.bahamarush.model.BaseModel;
import rush.bahama.com.bahamarush.utils.OnDataBuildListener;
import rush.bahama.com.bahamarush.utils.OnDataLoadedListener;

/**
 * Created by amitn on 20.07.2015
 */
public abstract class ServiceListenerActivity<T extends BaseModel> extends ToolbarActivity implements OnDataBuildListener, OnDataLoadedListener<T> {

    private MaterialDialog mDialog;
    protected boolean mRoutesLoaded;
    protected boolean mToursLoaded;
    protected boolean mPlacesLoaded;
    protected MainService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDialog = new MaterialDialog.Builder(this)
                .progress(true, 0, false)
                .cancelable(false)
                .title(R.string.refreshing)
                .content(R.string.loading_data)
                .build();
        if (!isMyServiceRunning()) {
            startService(new Intent(this, MainService.class));
        }
    }

    private boolean isMyServiceRunning() {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (MainService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onStartBuildingData() {
        mRoutesLoaded = false;
        mToursLoaded = false;
        mPlacesLoaded = false;
        mDialog.show();
    }

    @Override
    public void onDataBuilt() {
        if (!mService.isLoading() && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        bindService(new Intent(this, MainService.class), mConnection, 0);
    }

    @Override
    protected void onDestroy() {
        if (mService != null) {
            try {
                unbindService(mConnection);
            } catch (IllegalStateException e) {
                // do nothing
            }
        }
        super.onDestroy();
    }


    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder binder) {
            mService = ((MainService.MyBinder)binder).getService();
            if (!mService.isInitiated()) {
                mService.initData(ServiceListenerActivity.this);
            }
            ServiceListenerActivity.this.onServiceConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
        }
    };

    protected abstract void onServiceConnected();

    protected void informError(String msg) {
        new MaterialDialog.Builder(this).cancelable(false).positiveText("OK").content(msg).show();
    }

    @Override
    public void onDataLoaded(List<T> items) {

    }

    @Override
    public String getParentObjectId() {
        return null;
    }
}
