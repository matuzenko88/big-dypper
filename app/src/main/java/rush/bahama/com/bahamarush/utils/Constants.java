package rush.bahama.com.bahamarush.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.concurrent.TimeUnit;

/**
 * Created by amitn on 20.07.2015
 */
public class Constants {

    /**
     * Parse settings
     */
    public static final String PARSE_ID = "CRbwKJyhJ9reawyGAm0Pga6gxjxAuRTeoBW1Zy9B";
    public static final String PARSE_KEY = "FujLwGnFlQQ2dZ211d8lJEjdVa8O2AvNoSD3my8z";
    /**
     * PayPal settings
     * */
    public static final String PAYPAL_ID = "ASzJwYY7dElH1mqyb3Vf-4Op--szJcqKe0nlFb-fvurmf3KkfZTE3qX2uK0692Ye9ppQX7iQ_WO9VnYW";
    public static final String PAYPAL_SECRET = "EMRzqHterUBlsevOFbPli2jP_iTXA-oxscbXCz4OLNhqfrtE0HKcbNI16dqNNXGQECRrHcDp7BZPHx_j";
    /**
     * Keys for Parse objects
     */
    public static final String CUSTOMER_DEVICE_CLASS = "CustomerDevice";
    public static final String NAME_KEY = "name";
    public static final String ORDER_KEY = "order";
    public static final String ROUTE_ID_KEY = "routeId";
    //public static final String DURATION_KEY = "duration";
    public static final String TOUR_ID_KEY = "tourId";
    public static final String POSITION_KEY = "position";
    public static final String ICON_KEY = "icon";
    public static final String DESCRIPTION_KEY = "description";
    public static final String AUDIO_KEY = "audio";
    public static final String EMAIL_KEY = "email";
    public static final String LOCATION_KEY = "location";
    public static final String DEVICE_ID_KEY = "deviceId";
    /**
     * Extras keys
     */
    public static final int INTENT_REQUEST_FINISH_SESSION = 8171;
    public static int REQUEST_PAYMENT = 67;
    public static final String EXTRA_PLACE_ID = "extras_place_id";
    public static final String EXTRA_TYPE = "extra_type";
    public static final String KEY_PWD = "pwd";
    public static final String STARTED_AT_KEY = "started_at";
    public static final String SESSION_TIME = "max_time";
    public static final String TOTAL_TOURS_AMOUNT = "total_tours_amount";
    public static final String LINK_TO_REFUND = "refund";
    /**
     * Directories and files
     */
    public static final String DEFAULT_DATA_DIR = "/Android/data/";
    public static final String ROUTES_CACHE_DIR = "/routes_cache/";
    public static final String PLACES_CACHE_DIR = "/places_cache/";
    public static final String AUDIO_FILE_NAME = "audio.mp3";
    public static final String ICON_FILE_NAME = "icon.jpg";
    public static final String XML = ".xml";
    /**
     * Numeric constants
     */
    public static final int WATCH_RADIUS = 30; // meters to near
    public static final int CURRENT_ROUTE_REFRESH_TIME = 10000; // route from current location to first unseen point refresh interval, milliseconds
    public static final int MAX_HOURS_OVERTIME = 3;
    public static final int USD_PER_HOUR_OVERTIME = 5;
    public static final int USD_ONE_TOUR_COST = 10;
    public static final int USD_FULL_INSURANCE_COST = 115; // 100 + (3 * 5) for overtime
    /**
     * GMaps constants
     */
    private static final String MAPS_1 = "http://maps.googleapis.com/maps/api/directions/xml?origin=";
    private static final String MAPS_2 = "&destination=";
    private static final String MAPS_3 = "&sensor=false&units=metric&mode=driving";

    public static String gMapsURL(LatLng start, LatLng end) {
        return MAPS_1 + start.latitude + "," + start.longitude + MAPS_2 + end.latitude + "," + end.longitude + MAPS_3;
    }

    /**
     * Utils methods
     * */
    public static String getDurationString(long sec, boolean showSeconds) {
        long hours = TimeUnit.SECONDS.toHours(sec);
        long minutes = TimeUnit.SECONDS.toMinutes(sec) -
                TimeUnit.HOURS.toMinutes(hours);
        long seconds = sec - TimeUnit.HOURS.toSeconds(hours) - TimeUnit.MINUTES.toSeconds(minutes);
        StringBuilder result = new StringBuilder();
        if (hours > 0) {
            result.append(String.format("%02d hr", hours));
        }
        if (minutes > 0 || showSeconds) {
            result.append(" ").append(String.format("%02d min", minutes));
        }
        if (showSeconds) {
            result.append(" ").append(String.format("%02d sec", seconds));
        }
        return result.toString();
    }
}
