package rush.bahama.com.bahamarush.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rush.bahama.com.bahamarush.core.MainService;
import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.core.MyApplication;
import rush.bahama.com.bahamarush.model.BaseModel;
import rush.bahama.com.bahamarush.model.RouteModel;
import rush.bahama.com.bahamarush.model.TourModel;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.utils.OnPayPalActionCallback;

/**
 * Created by amitn on 06.07.2015
 */
public class DataListActivity extends ServiceListenerActivity<BaseModel> {

    private MainService.ObjectType mType;
    private String mRouteId;
    @Bind(R.id.lv_routes)
    ListView mListView;
    @Bind(R.id.btn_finish_tour)
    View btnFinishTour;
    private String mCurrentTour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mType = (MainService.ObjectType) extras.getSerializable(Constants.EXTRA_TYPE);
            if (mType == MainService.ObjectType.TOUR) {
                setTitle(getString(R.string.select_tour));
                mRouteId = extras.getString(Constants.ROUTE_ID_KEY, null);
            } else {
                setTitle(getString(R.string.transportation));
            }
        }
        mListView.setAdapter(new RoutesAdapter<>(this, R.layout.list_row, new ArrayList<BaseModel>()));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final RoutesAdapter<BaseModel> adapter = (RoutesAdapter) parent.getAdapter();
                BaseModel item = adapter.getItem(position);
                if (mService.hasDataForObject(mType, item.getParseId())) {
                    if (item instanceof RouteModel) {
                        Intent i = new Intent(DataListActivity.this, DataListActivity.class);
                        i.putExtra(Constants.ROUTE_ID_KEY, item.getParseId());
                        i.putExtra(Constants.EXTRA_TYPE, MainService.ObjectType.TOUR);
                        startActivity(i);
                        finish();
                    } else if (item instanceof TourModel) {
                        mCurrentTour = item.getParseId();
                        mService.getPayPalToken();
                        PayPalPayment payment = new PayPalPayment(new BigDecimal(Constants.USD_ONE_TOUR_COST), "USD", item.getName() + " (" + item.getParseId() + ")",
                                PayPalPayment.PAYMENT_INTENT_SALE);
                        Intent intent = new Intent(DataListActivity.this, PaymentActivity.class);
                        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, MyApplication.getPayPalConfig());
                        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
                        startActivityForResult(intent, Constants.REQUEST_PAYMENT);
                    }
                } else {
                    Toast.makeText(DataListActivity.this,
                            getString(R.string.thiz)
                                    + " "
                                    + getString(mType.getParseNameRes()).toLowerCase(Locale.getDefault())
                                    + " "
                                    + getString(R.string.is_empty),
                            Toast.LENGTH_SHORT)
                            .show();
                }

            }
        });
        if (mType == MainService.ObjectType.TOUR) {
            startPayPalService();
        }
    }

    public void startTour() {
        if (mCurrentTour != null) {
            mService.startNewTour(mCurrentTour);
            startActivity(new Intent(DataListActivity.this, TourActivity.class));
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        if (mType == MainService.ObjectType.TOUR) {
            stopService(new Intent(this, PayPalService.class));
        }
        super.onDestroy();
    }

    private void startPayPalService() {
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, MyApplication.getPayPalConfig());
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_PAYMENT) {
            if (resultCode == RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        checkPaymentAndStart(confirm.toJSONObject().getJSONObject("response").getString("id"));
                    } catch (JSONException e) {
                        informError(getString(R.string.err_json) + " " + e.getMessage());
                    }
                } else {
                    informError(getString(R.string.err_server_answer));
                }
            } else if (resultCode == RESULT_CANCELED) {
                informError(getString(R.string.msg_payment_canceled));
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                informError(getString(R.string.msg_wrong_payment));
            }
        } else if(requestCode == Constants.INTENT_REQUEST_FINISH_SESSION){
            if (resultCode == RESULT_OK) {
                startActivity(new Intent(this, SplashActivity.class));
                finish();
            }
        }
    }

    private void checkPaymentAndStart(String id) {
        final MaterialDialog d = new MaterialDialog.Builder(this).cancelable(false).progress(true, 0).content(getString(R.string.msg_check_payment)).show();
        mService.checkPayPalPayment(id, new OnPayPalActionCallback() {
            @Override
            public void onSuccess() {
                d.dismiss();
                startTour();
            }

            @Override
            public void onError(String error) {
                d.dismiss();
                informError(error);
            }
        }, false);
    }

    @OnClick(R.id.btn_finish_tour)
    public void finishTourClick() {
        MainService.showFinishTourDialog(this);
    }

    @Override
    protected void informError(String msg) {
        super.informError(msg);
        mCurrentTour = null;
    }

    @Override
    protected void onServiceConnected() {
        mService.setOnDataLoadedListener(mType, this);
        if (!mService.isSessionActive()) {
            btnFinishTour.setVisibility(View.GONE);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_data_list;
    }

    @Override
    public void onBackPressed() {
        if (mType == MainService.ObjectType.TOUR) {
            Intent i = new Intent(DataListActivity.this, DataListActivity.class);
            i.putExtra(Constants.EXTRA_TYPE, MainService.ObjectType.ROUTE);
            startActivity(i);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onDataLoaded(List<BaseModel> items) {
        RoutesAdapter a = (RoutesAdapter) mListView.getAdapter();
        Collections.sort(items);
        a.clear();
        a.addAll(items);
        a.notifyDataSetChanged();
    }

    @Override
    public String getParentObjectId() {
        return mRouteId;
    }

    class RoutesAdapter<T extends BaseModel> extends ArrayAdapter<T> {

        public RoutesAdapter(Context context, int resource, List<T> objects) {
            super(context, resource, objects);
        }

        class ViewHolder {

            @Bind(R.id.row_title)
            TextView title;
            @Bind(R.id.row_description)
            TextView description;

            private ViewHolder(View v) {
                ButterKnife.bind(this, v);
            }
        }

        @SuppressWarnings("unchecked")
        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.list_row, null);
                vh = new ViewHolder(convertView);
                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }
            BaseModel item = getItem(position);
            vh.title.setText(item.getName());
            if (item.getDescription() != null && !item.getDescription().isEmpty()) {
                vh.description.setVisibility(View.VISIBLE);
                vh.description.setText(item.getDescription());
            } else {
                vh.description.setVisibility(View.GONE);
            }
            return convertView;
        }
    }
}
