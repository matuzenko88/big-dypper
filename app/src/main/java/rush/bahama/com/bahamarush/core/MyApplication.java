package rush.bahama.com.bahamarush.core;

import android.app.Application;
import android.provider.Settings;
import com.parse.Parse;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import rush.bahama.com.bahamarush.utils.Constants;

/**
 * Created by amitn on 25.06.2015
 */
public class MyApplication extends Application {

    private static PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(Constants.PAYPAL_ID);

    public static PayPalConfiguration getPayPalConfig() {
        return config;
    }
    public static String DEVICE_ID;

    @Override
    public void onCreate() {
        super.onCreate();
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, Constants.PARSE_ID, Constants.PARSE_KEY);
        DEVICE_ID = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

}
