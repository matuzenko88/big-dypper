package rush.bahama.com.bahamarush.core;

import android.app.Activity;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import rush.bahama.com.bahamarush.R;
import rush.bahama.com.bahamarush.model.PlaceModel;
import rush.bahama.com.bahamarush.model.RouteModel;
import rush.bahama.com.bahamarush.model.TourModel;
import rush.bahama.com.bahamarush.ui.SessionEndActivity;
import rush.bahama.com.bahamarush.ui.TourActivity;
import rush.bahama.com.bahamarush.utils.Constants;
import rush.bahama.com.bahamarush.utils.OnDataBuildListener;
import rush.bahama.com.bahamarush.utils.OnDataLoadedListener;
import rush.bahama.com.bahamarush.utils.OnPayPalActionCallback;
import rush.bahama.com.bahamarush.utils.OnTourTimerListener;

/**
 * Created by amitn on 20.07.2015
 */
public class MainService extends Service {

    private IBinder mBinder = new MyBinder();
    public static final int NOTIFICATION_ID = 1086;
    private Map<ObjectType, OnDataLoadedListener> mListeners;
    private List<RouteModel> mRoutes;
    private Map<String, Set<TourModel>> mTours;
    private Map<String, Set<PlaceModel>> mPlaces;
    private boolean mPlacesHasBeenUpdated;
    private boolean mToursHasBeenUpdated;
    private boolean mRoutesHasBeenUpdated;
    private boolean mQueryingRoutes;
    private boolean mQueryingTours;
    private boolean mQueryingPlaces;
    private OnDataBuildListener mDataBuildListener;
    private OnTourTimerListener mTimerListener;
    private NotificationCompat.Builder mNotificationBuilder;
    private Session mSession;
    private String ppToken;
    private String mLinkToRefund;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(getApplicationContext(), TourActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        mNotificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Bahama Rush")
                .setShowWhen(false)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setOngoing(true);
        restoreSession();
        return START_STICKY;
    }

    private void restoreSession() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        long startedAt = prefs.getLong(Constants.STARTED_AT_KEY, 0);
        if (startedAt != 0) {
            mSession = new Session(prefs.getString(Constants.TOUR_ID_KEY, null),
                    startedAt,
                    prefs.getInt(Constants.TOTAL_TOURS_AMOUNT, 0),
                    prefs.getString(Constants.LINK_TO_REFUND, null));
        }
    }

    public void getPayPalToken() {
        Map<String, String> map = new HashMap<>();
        map.put("auth", Base64.encodeToString((Constants.PAYPAL_ID + ":" + Constants.PAYPAL_SECRET).getBytes(Charset.forName("UTF-8")), Base64.DEFAULT).replace("\n", ""));
        ParseCloud.callFunctionInBackground("getPayPalToken", map, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                    return;
                }
                try {
                    JSONObject j = new JSONObject((String) o);
                    ppToken = j.getString("access_token");
                    Log.e("TOKEN SUCCESS!", "" + ppToken);
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

            }
        });
    }

    public void checkPayPalPayment(final String id, final OnPayPalActionCallback listener, final boolean isInsurance) {
        Log.e("PAY_ID", id);
        Map<String, String> map = new HashMap<>();
        map.put("token", ppToken);
        map.put("pid", id.replace("\n", ""));
        ParseCloud.callFunctionInBackground("checkPayPalPayment", map, new FunctionCallback<Object>() {
            @Override
            public void done(Object o, ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                    listener.onError(getString(R.string.pay_error) + " " + e.getMessage());
                    return;
                }
                try {
                    Log.e("SUCCESS!", "" + ((String) o).replace("{\"", "{\n\"").replace(",", ",\n").replace("\"}", "\"\n}"));
                    JSONObject j = new JSONObject((String) o);
                    JSONObject tr = j.getJSONArray("transactions").getJSONObject(0);
                    String linkToRefund = null;
                    JSONArray links = tr.getJSONArray("related_resources").getJSONObject(0).getJSONObject("sale").getJSONArray("links");
                    for (int i = 0; i < links.length(); i++) {
                        JSONObject l = links.getJSONObject(i);
                        if ("refund".equals(l.getString("rel"))) {
                            linkToRefund = l.getString("href");
                            if (isInsurance) {
                                mLinkToRefund = linkToRefund;
                                showForeground("Choose your tour.");
                            }
                            break;
                        }
                    }
                    JSONObject amount = tr.getJSONObject("amount");
                    SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(MainService.this);
                    ParseObject po = new ParseObject("Payment");
                    po.put(Constants.DEVICE_ID_KEY, p.getString(Constants.DEVICE_ID_KEY, ""));
                    po.put(Constants.NAME_KEY, p.getString(Constants.NAME_KEY, ""));
                    po.put("amount", amount.getString("total") + " " + amount.getString("currency"));
                    JSONObject payer = j.getJSONObject("payer");
                    String pInfo = "Unknown";
                    try {
                        if ("credit_card".equals(payer.getString("payment_method"))) {
                            JSONObject cc = j.getJSONObject("payer").getJSONArray("funding_instruments").getJSONObject(0);
                            try {
                                JSONObject card = cc.getJSONObject("credit_card");
                                pInfo = card.getString("type") + "-" + card.getString("number").replace("x", "");
                            } catch (JSONException je) {
                                JSONObject cardToken = cc.getJSONObject("credit_card_token");
                                pInfo = cardToken.getString("type") + "-" + cardToken.getString("last4");
                            }
                        } else {
                            pInfo = payer.getJSONObject("payer_info").getString("email");
                        }
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    po.put("payer", pInfo);
                    po.put("description", tr.getString("description"));
                    po.put("paymentId", id);
                    po.put("saleId", linkToRefund);
                    po.save();
                    listener.onSuccess();
                } catch (JSONException | ParseException e1) {
                    e1.printStackTrace();
                    listener.onError(getString(R.string.err_verify_payment));
                }
            }
        });
    }

    private void refundPayPalInsurance(final OnPayPalActionCallback callback) {
        if (mSession.mLinkToInsurance != null) {
            Map<String, Object> map = new HashMap<>();
            JSONObject body = new JSONObject();
            int ref = getRefundableAmount();
            if (ref < Constants.USD_FULL_INSURANCE_COST) {
                try {
                    JSONObject am = new JSONObject();
                    am.put("currency", "USD");
                    am.put("total", getRefundableAmount());
                    body.put("amount", am);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            map.put("token", ppToken);
            map.put("link", mSession.mLinkToInsurance);
            map.put("body", body.toString());
            ParseCloud.callFunctionInBackground("refundInsurance", map, new FunctionCallback<Object>() {
                @Override
                public void done(Object o, ParseException e) {
                    if (e != null) {
                        callback.onError(getString(R.string.err) + " " + e.getMessage());
                    } else {
                        callback.onSuccess();
                    }
                }
            });
        } else {
            callback.onError(getString(R.string.err_session_fail));
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void initData(OnDataBuildListener doneListener) {
        mDataBuildListener = doneListener;
        mDataBuildListener.onStartBuildingData();
        mRoutes = new ArrayList<>();
        mTours = new HashMap<>();
        mPlaces = new HashMap<>();
        mListeners = new HashMap<>();
        reQueryRoutes();
        if (isOnline()) {
            refreshInBackground();
        } else {
            Toast.makeText(this, getString(R.string.using_cache), Toast.LENGTH_SHORT).show();
            doneListener.onDataBuilt();
        }
    }

    public boolean isInitiated() {
        return mTours != null && mPlaces != null && mRoutes != null;
    }

    public boolean isLoading() {
        return mQueryingPlaces || mQueryingRoutes || mQueryingTours;
    }

    private boolean isOnline() {
        boolean isOnline = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isAvailable()
                && activeNetwork.isConnectedOrConnecting()) {
            isOnline = true;
        }
        return isOnline;
    }

    private void reQueryRoutes() {
        mRoutesHasBeenUpdated = false;
        mQueryingRoutes = true;
        ParseQuery<ParseObject> routesQuery = new ParseQuery<>(getString(ObjectType.ROUTE.getParseNameRes()));
        routesQuery.fromLocalDatastore();
        routesQuery.findInBackground(new FindCallback<ParseObject>() {
            @SuppressWarnings("unchecked")
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (mRoutesHasBeenUpdated) {
                    reQueryRoutes();
                    return;
                }
                if (e != null || list == null || list.isEmpty()) {
                    mQueryingRoutes = false;
                    return;
                }
                mRoutes.clear();
                for (ParseObject o : list) {
                    mRoutes.add(RouteModel.fromParseObject(o));
                }
                Collections.sort(mRoutes);
                OnDataLoadedListener<RouteModel> listener = mListeners.get(ObjectType.ROUTE);
                if (!mQueryingTours) {
                    reQueryTours();
                }
                mQueryingRoutes = false;
                if (listener != null) {
                    listener.onDataLoaded(mRoutes);
                }
                notifyDataBuildListener();
            }
        });
    }

    private void reQueryTours() {
        mToursHasBeenUpdated = false;
        mQueryingTours = true;
        ParseQuery<ParseObject> toursQuery = new ParseQuery<>(getString(ObjectType.TOUR.getParseNameRes()));
        toursQuery.fromLocalDatastore();
        toursQuery.findInBackground(new FindCallback<ParseObject>() {
            @SuppressWarnings("unchecked")
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (mToursHasBeenUpdated) {
                    reQueryTours();
                    return;
                }
                if (e != null || list == null || list.isEmpty()) {
                    mQueryingTours = false;
                    return;
                }
                mTours.clear();
                for (ParseObject o : list) {
                    TourModel tour = TourModel.fromParseObject(o);
                    if (!mTours.containsKey(tour.getRouteId())) {
                        mTours.put(tour.getRouteId(), new HashSet<TourModel>());
                    }
                    mTours.get(tour.getRouteId()).add(tour);
                }
                mQueryingTours = false;
                if (!mQueryingPlaces) {
                    reQueryPlaces();
                }
                OnDataLoadedListener<TourModel> listener = mListeners.get(ObjectType.TOUR);
                if (listener != null && mTours.containsKey(listener.getParentObjectId())) {
                    listener.onDataLoaded(new ArrayList<>(mTours.get(listener.getParentObjectId())));
                }
                notifyDataBuildListener();
            }
        });
    }

    private void reQueryPlaces() {
        mPlacesHasBeenUpdated = false;
        mQueryingPlaces = true;
        ParseQuery<ParseObject> routesQuery = new ParseQuery<>(getString(ObjectType.PLACE.getParseNameRes()));
        routesQuery.fromLocalDatastore();
        routesQuery.findInBackground(new FindCallback<ParseObject>() {
            @SuppressWarnings("unchecked")
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (mPlacesHasBeenUpdated) {
                    reQueryPlaces();
                    return;
                }
                if (e != null || list == null || list.isEmpty()) {
                    mQueryingPlaces = false;
                    return;
                }
                mPlaces.clear();
                for (ParseObject o : list) {
                    PlaceModel place = PlaceModel.fromParseObject(o, getApplicationContext());
                    if (!mPlaces.containsKey(place.getTourId())) {
                        mPlaces.put(place.getTourId(), new HashSet<PlaceModel>());
                    }
                    mPlaces.get(place.getTourId()).add(place);
                }
                mQueryingPlaces = false;
                OnDataLoadedListener<PlaceModel> listener = mListeners.get(ObjectType.PLACE);
                if (listener != null && mPlaces.containsKey(listener.getParentObjectId())) {
                    listener.onDataLoaded(new ArrayList<>(mPlaces.get(listener.getParentObjectId())));
                }
                notifyDataBuildListener();
            }
        });
    }

    @SuppressWarnings("unchecked")
    private void refreshInBackground() {
        for (ObjectType t : ObjectType.values()) {
            ParseQuery<ParseObject> q = new ParseQuery<>(getString(t.getParseNameRes()));
            q.findInBackground(getCallback(t));
        }
    }

    private FindCallback<ParseObject> getCallback(final ObjectType type) {
        return new FindCallback<ParseObject>() {
            @Override
            public void done(final List<ParseObject> list, ParseException e) {
                if (e != null || list == null || list.isEmpty()) {
                    notifyDataBuildListener();
                    return;
                }
                ParseObject.unpinAllInBackground(type.toString(), new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        try {
                            ParseObject.pinAll(type.toString(), list);
                            switch (type) {
                                case ROUTE:
                                    mRoutesHasBeenUpdated = true;
                                    if (!mQueryingRoutes) {
                                        reQueryRoutes();
                                    }
                                    break;
                                case TOUR:
                                    mToursHasBeenUpdated = true;
                                    if (!mQueryingTours) {
                                        reQueryTours();
                                    }
                                    break;
                                case PLACE:
                                    mPlacesHasBeenUpdated = true;
                                    if (!mQueryingPlaces) {
                                        reQueryPlaces();
                                    }
                                    break;
                            }
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                });
            }
        };
    }

    private void notifyDataBuildListener() {
        if (mDataBuildListener != null) {
            try {
                mDataBuildListener.onDataBuilt();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    @SuppressWarnings("unchecked")
    public void setOnDataLoadedListener(ObjectType type, @NonNull OnDataLoadedListener onDataLoadedListener) {
        if (mListeners != null) {
            mListeners.put(type, onDataLoadedListener);
        }
        Collection c = null;
        switch (type) {
            case ROUTE:
                c = mRoutes;
                break;
            case TOUR:
                c = mTours.get(onDataLoadedListener.getParentObjectId());
                break;
            case PLACE:
                c = mPlaces.get(onDataLoadedListener.getParentObjectId());
                break;
        }
        if (c != null && !c.isEmpty()) {
            onDataLoadedListener.onDataLoaded(new ArrayList(c));
        }
    }

    public boolean hasDataForObject(ObjectType type, String routeId) {
        switch (type) {
            case ROUTE:
                return mTours != null && mTours.containsKey(routeId) || mQueryingTours;
            case TOUR:
                return mPlaces != null && mPlaces.containsKey(routeId) || mQueryingPlaces;
            default:
                return false;
        }
    }

    private TourModel findTour(String tourId) {
        if (tourId == null) {
            return null;
        }
        for (Set<TourModel> set : mTours.values()) {
            for (TourModel t : set) {
                if (t.getParseId().equals(tourId)) {
                    return t;
                }
            }
        }
        return null;
    }

    public PlaceModel findPlace(String placeId) {
        for (Set<PlaceModel> set : mPlaces.values()) {
            for (PlaceModel p : set) {
                if (p.getParseId().equals(placeId)) {
                    return p;
                }
            }
        }
        return null;
    }

    public int getTotalAmount() {
        return mSession.getTotalAmount();
    }

    public int getRefundableAmount() {
        return Constants.USD_FULL_INSURANCE_COST - mSession.getAdditionalAmount();
    }

    public void showForeground(String message) {
        mNotificationBuilder.setContentText(message);
        Notification notification = mNotificationBuilder.build();
        startForeground(NOTIFICATION_ID, notification);
    }

    public void startNewTour(@NonNull String tourId) {
        if (mSession == null) {
            mSession = new Session(tourId, mLinkToRefund);
            mLinkToRefund = null;
        } else {
            mSession.setTourId(tourId);
        }
    }

    public void endCurrentTour() {
        if (mSession != null) {
            mSession.setTourId(null);
        }
    }

    public void endCurrentSession(final OnPayPalActionCallback callback) {
        refundPayPalInsurance(new OnPayPalActionCallback() {
            @Override
            public void onSuccess() {
                endCurrentTour();
                mSession.end();
                mSession = null;
                callback.onSuccess();
            }

            @Override
            public void onError(String error) {
                callback.onError(error);
            }
        });
    }

    public TourModel getCurrentTour() {
        return mSession == null ? null : findTour(mSession.mTourId);
    }

    public void setTimerListener(TourActivity timerListener) {
        this.mTimerListener = timerListener;
    }

    public boolean isSessionActive() {
        return mSession != null && (mLinkToRefund != null || mSession.mLinkToInsurance != null);
    }

    public enum ObjectType {
        ROUTE(R.string.route), TOUR(R.string.tour), PLACE(R.string.place);

        ObjectType(int parseName) {
            mParseName = parseName;
        }

        private int mParseName;

        public int getParseNameRes() {
            return mParseName;
        }
    }

    public class MyBinder extends Binder {
        public MainService getService() {
            return MainService.this;
        }
    }

    private class Session {
        private CountDownTimer mTimer;
        long mStartedAt;
        String mTourId;
        SharedPreferences.Editor mEditor;
        int hoursOver;
        int mTotalToursAmount;
        String mLinkToInsurance;

        // new tour
        Session(String tourId, String linkToRefund) {
            this(tourId, System.currentTimeMillis(), Constants.USD_ONE_TOUR_COST, linkToRefund);
        }

        // restored tour
        Session(String tourId, long startedAt, int totalAmount, String linkToInsurance) {
            mTourId = tourId;
            mStartedAt = startedAt;
            mTotalToursAmount = totalAmount;
            mLinkToInsurance = linkToInsurance;
            mEditor = PreferenceManager.getDefaultSharedPreferences(MainService.this).edit();
            save();
            start();
        }

        private void start() {
            int maxRentTimeHours = PreferenceManager.getDefaultSharedPreferences(MainService.this).getInt(Constants.SESSION_TIME, 0);
            long endTime = mStartedAt + TimeUnit.HOURS.toMillis(maxRentTimeHours);
            long now = System.currentTimeMillis();
            while (endTime < now) {
                endTime += TimeUnit.HOURS.toMillis(1);
                hoursOver++;
            }
            if (hoursOver > Constants.MAX_HOURS_OVERTIME) {
                MainService.this.onFullOverTime();
            } else {
                restartTimer(endTime - now);
                if (hoursOver > 0) {
                    MainService.this.onOverTime(hoursOver);
                }
            }

        }

        private void restartTimer(long endTimeMillis) {
            if (mTimer != null) {
                mTimer.cancel();
            }
            mTimer = new CountDownTimer(endTimeMillis, 1000) {
                @Override
                public void onTick(long timeLeft) {
                    String time = timeLeft > 0 ? Constants.getDurationString(TimeUnit.MILLISECONDS.toSeconds(timeLeft), true) : getString(R.string.time_over_header);
                    if (mTimerListener != null) {
                        mTimerListener.onTick(time, hoursOver > 0);
                    }
                    showForeground(time);
                }

                @Override
                public void onFinish() {
                    hoursOver++;
                    if (hoursOver <= Constants.MAX_HOURS_OVERTIME) {
                        restartTimer(TimeUnit.HOURS.toMillis(1));
                        MainService.this.onOverTime(hoursOver);
                        mEditor.apply();
                    } else {
                        mTimer.cancel();
                        MainService.this.onFullOverTime();
                    }
                }
            }.start();
        }


        private void save() {
            mEditor.putString(Constants.TOUR_ID_KEY, mTourId);
            mEditor.putLong(Constants.STARTED_AT_KEY, mStartedAt);
            mEditor.putInt(Constants.TOTAL_TOURS_AMOUNT, mTotalToursAmount);
            mEditor.putString(Constants.LINK_TO_REFUND, mLinkToInsurance);
            mEditor.apply();
        }

        void end() {
            if (mTimer != null) {
                mTimer.cancel();
            }
            stopForeground(true);
            mEditor.remove(Constants.TOUR_ID_KEY);
            mEditor.remove(Constants.STARTED_AT_KEY);
            mEditor.remove(Constants.TOTAL_TOURS_AMOUNT);
            mEditor.remove(Constants.LINK_TO_REFUND);
            mEditor.apply();
        }

        void setTourId(String tourId) {
            mTourId = tourId;
            if (tourId != null) {
                mTotalToursAmount += Constants.USD_ONE_TOUR_COST;
            }
            mEditor.putInt(Constants.TOTAL_TOURS_AMOUNT, mTotalToursAmount);
            mEditor.putString(Constants.TOUR_ID_KEY, tourId);
            mEditor.apply();
        }

        public int getTotalAmount() {
            return mTotalToursAmount + getAdditionalAmount();
        }

        int getAdditionalAmount() {
            return hoursOver > Constants.MAX_HOURS_OVERTIME ? Constants.USD_FULL_INSURANCE_COST : (hoursOver * Constants.USD_PER_HOUR_OVERTIME);
        }
    }

    private void onFullOverTime() {
        if (mTimerListener != null) {
            mTimerListener.onTimeIsOver(Constants.MAX_HOURS_OVERTIME + 1);
        }
        // TODO bill for 100$
    }

    private void onOverTime(int hoursOver) {
        if (mTimerListener != null) {
            mTimerListener.onTimeIsOver(hoursOver);
        }
    }

    public static void showFinishTourDialog(final Activity context){
        new MaterialDialog.Builder(context)
                .content(R.string.back_in_base_header)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .positiveColorRes(R.color.orange_dark)
                .negativeColorRes(R.color.orange_dark)
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        context.startActivityForResult(new Intent(context, SessionEndActivity.class), Constants.INTENT_REQUEST_FINISH_SESSION);
                    }
                })
                .show();
    }

}
